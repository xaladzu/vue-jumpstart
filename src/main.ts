import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';
import VueResource from 'vue-resource';
import Vuelidate from 'vuelidate';
import Vuetify from 'vuetify';


Vue.config.productionTip = false;
Vue.use(VueResource);
Vue.use(Vuelidate);
Vue.use(Vuetify);
Vue.http.options.root = 'https://vuejs-app-e0036.firebaseio.com/';
Vue.filter('currency', (value: any) => {
  return `${value.toLocaleString()}$`;
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
