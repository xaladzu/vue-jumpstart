import Vue from "vue";
import Vuex from "vuex";

import stocks from "./modules/stocks.module";
import portfolio from "./modules/portfolio.module";
import * as actions from "./actions";

Vue.use(Vuex);
export const state = {
    showModal: false,
    showToast: false
};
export default new Vuex.Store({
    state,
    actions,
    mutations: {
        'SET_MODAL'(state: any, payload: any) {
            state.showModal = payload;
        },
        'SET_TOAST'(state: any, payload: any) {
            state.showToast = payload;
            setTimeout(() => {
                state.showToast = !payload;
            }, 2000);
        }
    },
    getters: {
        modalState() {
            return state.showModal;
        },
        toastState() {
            return state.showToast;
        }
    },
    modules: {
        stocks,
        portfolio
    }
});
