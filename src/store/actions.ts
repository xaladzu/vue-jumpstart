import Vue from "vue";

export const loadData = ({ commit }: { commit: any }) => {
  Vue.http.get("data.json")
  .then((res: any) => res.json()
  .then((data: any) => {
    if (data) {
        const stocks = data.stocks;
        const founds = data.founds;
        const stockPortfolio = data.stockPortfolio;

        const portfolio = {
            stockPortfolio,
            founds
        };
        commit('SET_STOCKS', stocks);
        commit('SET_PORTFOLIO', portfolio);
    }
  }));
};

export const showModal = ({ commit }: { commit: any}, payload: any) => {
  commit('SET_MODAL', payload);
};
export const showToast = ({ commit }: { commit: any}, payload: any) => {
  commit('SET_TOAST', payload);
};
