import stocks from '../../data/data';

const state: any = {
    stocks: [],
};

const mutations = {
    'SET_STOCKS' (state: any, payload: any) {
        state.stocks = payload;
    },
    'RND_STOCKS' (state: any) {
        state.stocks.forEach((element: any) => {
            element.price = Math.round(element.price * (1 + Math.random() - 0.5));
        });
    }
};

const actions = {
    buyStock: ({ commit }: { commit: any}, payload: any) => {
        commit('BUY_STOCK', payload);
    },
    initStocks: ({ commit }: { commit: any}) => {
        commit('SET_STOCKS', stocks);
    },
    randomizeStocks: ({ commit }: {commit: any}) => {
        commit('RND_STOCKS');
    },
};

const getters = {
    stocks: (state: any) => {
        return state.stocks;
    },
};

export default {
    state,
    mutations,
    actions,
    getters,
};