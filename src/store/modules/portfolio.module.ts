const state = {
    founds: 10000,
    stocks: []
};

const mutations = {
    'BUY_STOCK' (state: any, { stockId, quantity, stockPrice }: { stockId: any, quantity: any, stockPrice: any}) {
        const record = state.stocks.find((e: any) => e.id === stockId);
        if (record) {
            record.quantity += quantity;
        } else {
            state.stocks.push({
                id: stockId,
                quantity: quantity,
            });
        }
        state.founds -= stockPrice * quantity;
    },
    'SELL_STOCK' (state: any, { stockId, quantity, stockPrice }: { stockId: any, quantity: any, stockPrice: any}) {
        const record = state.stocks.find((e: any) => e.id === stockId);
        if (record.quantity > quantity) {
            record.quantity -= quantity;
        } else {
            state.stocks.splice(state.stocks.indexOf(record), 1);
        }
        state.founds += stockPrice * quantity;
    },
    'SET_PORTFOLIO' (state: any, portfolio: any) {
        state.founds = portfolio.founds;
        state.stocks = portfolio.stockPortfolio ? portfolio.stockPortfolio : [];
    }
};

const actions = {
    sellStock: ({ commit }: { commit: any}, payload: any) => {
        commit('SELL_STOCK', payload);
    },
};

const getters = {
    stockPortfolio (state: any, getters: any) {
        return state.stocks.map((stock: any) => {
            const record = getters.stocks.find((e: any) => e.id === stock.id);
            return {
                id: stock.id,
                quantity: stock.quantity,
                name: record.name,
                price: record.price,
            };
        });
    },
    founds (state: any) {
        return state.founds;
    }
}

export default {
    state,
    mutations,
    actions,
    getters,
};
